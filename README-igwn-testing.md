# IGWN Testing Yum Repository

The IGWN Testing Yum Repository contains RPMS for production-ready
approved software produced by IGWN member groups.

See <https://computing.docs.ligo.org/guide/software/> for details on
IGWN Software practices.
