Name:      igwn-testing-config
Version:   8
Release:   4%{?dist}
Summary:   Yum/DNF configuration for IGWN Testing Repository

License:   GPLv3+
BuildArch: noarch
Requires:  redhat-release >= %{version}
Url: https://software.igwn.org/lscsoft/rocky/%{version}/testing/

Source0:   igwn-testing.repo
Source1:   COPYING
Source2:   README-igwn-testing.md

Provides:  lscsoft-testing-config = %{version}-%{release}
Obsoletes: lscsoft-testing-config < 8-6

%description
Yum/DNF configuation for IGWN Testing Repository on Rocky Linux %{version}

%prep
%setup -q -c -T
install -pm 644 %{SOURCE1} .
install -pm 644 %{SOURCE2} .

%build

%install
install -dm 755 %{buildroot}%{_sysconfdir}/yum.repos.d
install -pm 644 %{SOURCE0} %{buildroot}%{_sysconfdir}/yum.repos.d

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc README-igwn-testing.md
%license COPYING
%config(noreplace) %{_sysconfdir}/yum.repos.d/igwn-testing.repo


# dates should be formatted using: 'date +"%a %b %d %Y"'
%changelog
* Wed Feb 01 2023 Adam Mercer <adam.mercer@ligo.org> 8-4
- add source repository

* Wed Jan 04 2023 Adam Mercer <adam.mercer@ligo.org> 8-3
- rename to igwn-testing-config

* Mon Aug 02 2021 Adam Mercer <adam.mercer@ligo.org> 8-2
- update URL for Rocky Linux

* Fri Mar 20 2020 Adam Mercer <adam.mercer@ligo.org> 8-1
- update for CentOS8
